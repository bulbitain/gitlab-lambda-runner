# gitlab-lambda-runner

*Proof of concept for running gitlab builds via gitlab-runner on lambda.*

**DISCLAIMER:** This project is WIP and not intended for production use!

Please also check out my [Medium article](https://medium.com/@msvechla/serverless-gitlab-runner-builds-on-lambda-ded4b24b3c4f) on this topic for more details.

## Features

The following features are evaluated in this proof of concept:

- gitlab builds can be executed on lambda
- gitlab executor can inherit IAM permissions based on assigned role
- additional binaries and its dependencies can be executed during the build (e.g. terraform)

## Concept

We will use lambda-layers to embed [gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner) and its dependencies (e.g. git) to our custom lambda function. The custom golang lambda function will then execute gitlab-runner in the `run single` mode, using the `shell` executor. The custom lambda layers are built and uploaded with [img2lambda](https://github.com/awslabs/aws-lambda-container-image-converter). The git layer is provided by [lambci/git-lambda-layer](https://github.com/lambci/git-lambda-layer/).

This enables lightweight, local "one-off" builds inside the lambda function.

## Getting Started

To deploy gitlab runner to your aws account, use the examplary [deploy.sh](deploy.sh) script.

### Pre-requisites

To use the deploy.sh script, ensure you have the following dependencies installed:

- [aws-cli](https://aws.amazon.com/cli/)
- [docker](https://www.docker.com/)
- [img2lambda](https://github.com/awslabs/aws-lambda-container-image-converter)

The deploy script expects an existing lambda exection IAM role, which will be assigned to the gitlab runner. Create this role upfront via the CLI or console and supply it to the deploy script (see [Deployment](#Deployment)).

Additionally you will need an existing runner-token ([see docs](https://docs.gitlab.com/runner/register/)). An easy way of doing this is by executing `docker run -it --rm gitlab/gitlab-runner register`. **Be sure to register the runner with the `shell` executor!**

The token can the be retrieved from the gitlab settings CI/CD runners page.

### Deployment

Check out this git repository on your local machine and execute the [deploy.sh](deploy.sh) script, e.g.

```bash
./deploy.sh -r eu-central-1 -a arn:aws:iam::XXXXXXXXXXXX:role/lambda_execution_role -t myrunnerTokenxyz -u https://gitlab.com -o 300
```

For further usage instructions, see `./deploy.sh -h`.

## Evaluation

This proof of concept has been evaluated by applying terraform code, wich creates an s3 bucket, from a gitlab-ci pipeline (see [evaluation/](evaluation/) directory).

## Future Work

- figure out a way to automatically trigger the lambda function when new jobs are created
- fix `id` errors during pipeline execution (originating from `/etc/profile`, which is triggered by the `shell` executor)

## Acknowledgments

- [img2lambda](https://github.com/awslabs/aws-lambda-container-image-converter)
- [lambci/git-lambda-layer](https://github.com/lambci/git-lambda-layer/)